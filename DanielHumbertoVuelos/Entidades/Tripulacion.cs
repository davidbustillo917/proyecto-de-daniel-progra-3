﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Tripulacion
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public Aerolinea aerolinea { get; set; }
        public char rol { get; set; }
        public char estado { get; set; }
        public override string ToString()
        {
            return nombre;
        }
    }
}
