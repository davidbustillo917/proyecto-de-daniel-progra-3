﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Vuelo
    {
        public int id { get; set; }
        public Aerolinea aerolinea { get; set; }
        public double precio { get; set; }
        public DateTime fechaSalida { get; set; }
        public string aeropuertoSalida { get; set; }
        public DateTime fechaLlegada { get; set; }
        public string aeropuertoLlegada { get; set; }
        public Avion avion { get; set; }
        public Tripulacion tripulacion1 { get; set; }
        public Tripulacion tripulacion2 { get; set; }
        public Tripulacion tripulacion3 { get; set; }
        public Tripulacion tripulacion4 { get; set; }
        public Tripulacion tripulacion5 { get; set; }
        public double horas { get; set; }



    }
}
