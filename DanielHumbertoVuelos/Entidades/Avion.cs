﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Avion
    {
        public int id { get; set; }
        public string modelo { get; set; }
        public DateTime annoConstruccion { get; set; }
        public Aerolinea aerolinea { get; set; }
        public int capacidad { get; set; }
        public char estado { get; set; }

        public override string ToString()
        {
            return modelo;
        }
    }
}
