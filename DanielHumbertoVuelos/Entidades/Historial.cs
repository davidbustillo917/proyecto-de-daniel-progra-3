﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Historial
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string paisSalida { get; set; }
        public string paisLlegada { get; set; }
        public string paisEscala1 { get; set; }
        public string paisEscala2 { get; set; }
        public DateTime fechaCompra { get; set; }
        public double horas { get; set; }
        public double costoTotal { get; set; }

        public override string ToString()
        {
            return cedula;
        }
    }
}
