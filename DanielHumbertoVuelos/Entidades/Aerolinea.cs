﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Aerolinea
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public DateTime annoFundacion { get; set; }
        public char tipo { get; set; }
        public override string ToString()
        {
            return nombre;
        }

    }
}
