﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanielHumbertoVuelos.Entidades
{
    class Usuario
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string contrasena { get; set; }
        public char tipo { get; set; }
        public override string ToString()
        {
            return nombre;
        }

    }
}
