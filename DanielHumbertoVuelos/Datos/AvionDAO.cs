﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using Npgsql;

namespace DanielHumbertoVuelos.Datos
{
    class AvionDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Avion avion)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) " +
                    "VALUES ('" + avion.modelo+ "', '" + avion.annoConstruccion+ "', '" + avion.aerolinea.id + "', '" + avion.capacidad + "', '" + avion.estado + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<Avion> seleccionar()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Avion> aviones = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM aviones", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    aviones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return aviones;
        }

        public List<Avion> seleccionarParaRandom(int id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Avion> aviones = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM aviones where id_aerolinea = '" + id + "' and estado = 'D' limit 1", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    aviones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return aviones;
        }

        public bool CambiarEstadoAvion(int id)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("UPDATE aviones SET estado = 'O' where id = '" + id + "'", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public List<Avion> cargarAvionesPorRanking()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Avion> aviones = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT TA.id, TA.modelo, TA.ano_construccion, TA.id_aerolinea, TA.capacidad, TA.estado FROM aviones TA join vuelos TB on TA.id = TB.id_avion limit 5", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    aviones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return aviones;
        }


        public Avion cargar(NpgsqlDataReader reader)
        {
            Avion avion = new Avion
            {
                id = reader.GetInt16(0),
                modelo = reader.GetString(1),
                annoConstruccion = reader.GetDateTime(2),
                aerolinea = new AerolineaDAO().cargarAerolinea(reader.GetInt32(3)),
                capacidad = reader.GetInt32(4),
                estado = reader.GetChar(5)
            };
            return avion;
        }

        public Avion cargarAvion(int id)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM aviones where id = '" + id + "'", conexionRetorno);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        return cargar(dr);
                    }
                }
                conexionRetorno.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }
            return null;
        }

        public Avion cargarAvionPorAerolinea(int id, int idAerolinea)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM aviones where id = '" + id + "' and id_aerolinea = '" + idAerolinea + "'", conexionRetorno);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        return cargar(dr);
                    }
                }
                conexionRetorno.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }
            return null;
        }

        public List<Avion> seleccionarPorAerolinea(int id, int idAerolinea)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Avion> aviones = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM aviones where id = '" + id + "' and id_aerolinea = '" + idAerolinea + "'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    aviones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return aviones;
        }
    }
}
