﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using Npgsql;

namespace DanielHumbertoVuelos.Datos
{
    class TripulacionDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Tripulacion t)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) " +
                    "VALUES ('" + t.cedula + "', '" + t.nombre+ "', '" + t.fechaNacimiento+ "', '" + t.aerolinea.id + "', '" + t.rol+ "', '" + t.estado + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool CambiarEstado(int id)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("UPDATE tripulacion SET estado = 'O' where id = '" + id + "'", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<Tripulacion> seleccionar()
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                List<Tripulacion> tripulaciones = new List<Tripulacion>();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado FROM tripulacion", conexionRetorno);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        tripulaciones.Add(cargar(dr));
                    }
                }
                conexionRetorno.Close();
                return tripulaciones;
            }
            catch
            {
                
                return null;
            }
            
        }

        public List<Tripulacion> seleccionarPiloto(int id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Tripulacion> tripulaciones = new List<Tripulacion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado FROM tripulacion where id_aerolinea = '" + id + "' and rol = 'P' and estado = 'D'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    tripulaciones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return tripulaciones;
        }
        public List<Tripulacion> seleccionarServicio(int id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Tripulacion> tripulaciones = new List<Tripulacion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado FROM tripulacion where id_aerolinea = '" + id + "' and rol = 'S' and estado = 'D'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    tripulaciones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return tripulaciones;
        }

        public Tripulacion cargar(NpgsqlDataReader reader)
        {
            Tripulacion tripulacion = new Tripulacion
            {
                id = reader.GetInt16(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
                fechaNacimiento = reader.GetDateTime(3),
                aerolinea = new AerolineaDAO().cargarAerolinea(reader.GetInt32(4)),
                rol = reader.GetChar(5),
                estado = reader.GetChar(6)
            };
            return tripulacion;
        }

        public Tripulacion cargarTripulacion(int id)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado FROM tripulacion where id = '" + id + "'", conexionRetorno);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        return cargar(dr);
                    }
                }
                conexionRetorno.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                conexionRetorno.Close();
            }
            return null;
        }

        public List<Tripulacion> cargarTripulacionPorAerolinea(int id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Tripulacion> tripulaciones = new List<Tripulacion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado FROM tripulacion where id_aerolinea = '" + id + "'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    tripulaciones.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return tripulaciones;
        }
    }
}
