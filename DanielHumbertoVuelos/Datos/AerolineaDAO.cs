﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using Npgsql;

namespace DanielHumbertoVuelos.Datos
{
    class AerolineaDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Aerolinea aero)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO aerolinea (nombre, ano_fundacion, tipo) " +
                    "VALUES ('" + aero.nombre + "', '" + aero.annoFundacion + "', '" + aero.tipo + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        

        //Metodo para mostrar la aerolinea en el datagrid
        public List<Aerolinea> seleccionar()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Aerolinea> colaboradores = new List<Aerolinea>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, ano_fundacion, tipo FROM aerolinea", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return colaboradores;
        }
        
        public Aerolinea cargar(NpgsqlDataReader reader)
        {
            Aerolinea a = new Aerolinea();
            a.id = reader.GetInt16(0);
            a.nombre = reader.GetString(1);
            a.annoFundacion = reader.GetDateTime(2);
            a.tipo = reader.GetChar(3);
            return a;
        }

        //Metodo para cagar la aerolinea en otro objeto
        public Aerolinea cargarAerolinea(int id)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, ano_fundacion, tipo FROM aerolinea where id = '" + id + "'", conexionRetorno);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        return cargar(dr);
                    }
                }
                conexionRetorno.Close();
                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                
            }
            return null;
        }
    }
}
