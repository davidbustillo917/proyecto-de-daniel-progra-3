﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using Npgsql;

namespace DanielHumbertoVuelos.Datos
{
    class VueloDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Vuelo v)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO vuelos (precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas) " +
                    "VALUES ('" + v.precio + "', '" + v.fechaSalida+ "', '" + v.aeropuertoSalida+ "', '" + v.fechaLlegada + "', '" + v.aeropuertoLlegada+ "', '" + v.aerolinea.id + "', '" + v.avion.id + "', '" + v.tripulacion1.id + "', '" + v.tripulacion2.id + "', '" + v.tripulacion3.id + "', '" + v.tripulacion4.id + "', '" + v.tripulacion5.id + "', '" + v.horas + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool insertarHistorial(Historial h)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO historial (cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_compra, horas, costo) " +
                    "VALUES ('" + h.cedula+ "', '" + h.paisSalida+ "', '" + h.paisLlegada+ "', '" + h.paisEscala1 + "', '" + h.paisEscala2 + "', '" + h.fechaCompra + "', '" + h.horas + "', '" + h.costoTotal + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<Vuelo> seleccionar()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public Vuelo cargar(NpgsqlDataReader reader)
        {
            Vuelo vuelos = new Vuelo
            {
                id = reader.GetInt16(0),
                precio = reader.GetInt32(1),
                fechaSalida = reader.GetDateTime(2),
                aeropuertoSalida = reader.GetString(3),
                fechaLlegada = reader.GetDateTime(4),
                aeropuertoLlegada = reader.GetString(5),
                aerolinea = new AerolineaDAO().cargarAerolinea(reader.GetInt32(6)),
                avion = new AvionDAO().cargarAvion(reader.GetInt32(7)),
                tripulacion1 = new TripulacionDAO().cargarTripulacion(reader.GetInt32(8)),
                tripulacion2 = new TripulacionDAO().cargarTripulacion(reader.GetInt32(9)),
                tripulacion3 = new TripulacionDAO().cargarTripulacion(reader.GetInt32(10)),
                tripulacion4 = new TripulacionDAO().cargarTripulacion(reader.GetInt32(11)),
                tripulacion5 = new TripulacionDAO().cargarTripulacion(reader.GetInt32(12)),
                horas = reader.GetDouble(13)
            };
            return vuelos;
        }

        public List<Vuelo> vuelosPorFecha(DateTime fecha1, DateTime fecha2)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos where fecha_salida between '" + fecha1 + "' and '" + fecha2 + "'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public List<Vuelo> vuelosPorAerolinea(int id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos where id_aerolinea = '" + id + "'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public List<Vuelo> escalas(string id)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos where aeropuerto_llegada = '" + id + "'", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }


        public List<Vuelo> cargarPorBusqueda(DateTime fechaSalida, DateTime fechaLlegada, string aero1, string aero2)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos where aeropuerto_salida = @salida and aeropuerto_llegada = @llegada and fecha_salida = '" + fechaSalida + "' and  fecha_llegada = '"  + fechaLlegada + "'", conexionRetorno);
            cmd.Parameters.AddWithValue("@salida", aero1);
            cmd.Parameters.AddWithValue("@llegada", aero2);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public List<Vuelo> cargarPorBusquedaInteligente(string aeroSalida, string aeroLlegada, DateTime fechaSalida1, DateTime fechaSalida2, DateTime fechaLlegada1, DateTime fechaLlegada2)
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos where aeropuerto_salida = @salida and aeropuerto_llegada = @llegada and fecha_salida between '" + fechaSalida1 + "' and  '" + fechaSalida2 + "' and fecha_llegada between '" + fechaLlegada1 + "' and '" + fechaLlegada2 + "'", conexionRetorno);
            cmd.Parameters.AddWithValue("@salida", aeroSalida);
            cmd.Parameters.AddWithValue("@llegada", aeroLlegada);

            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public List<Vuelo> seleccionarPorPrecio()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos order by precio", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }

        public List<Vuelo> seleccionarPorRapidez()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Vuelo> vuelos = new List<Vuelo>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion, id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas FROM vuelos order by horas", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    vuelos.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return vuelos;
        }
    }
}
