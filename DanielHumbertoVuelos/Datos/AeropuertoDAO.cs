﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using Npgsql;

namespace DanielHumbertoVuelos.Datos
{
    class AeropuertoDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Aeropuerto aeropuerto)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO aeropuertos (iata, nombre, pais) " +
                    "VALUES ('" + aeropuerto.iata+ "', '" + aeropuerto.nombre+ "', '" + aeropuerto.pais + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public List<Aeropuerto> seleccionar()
        {
            conexionRetorno = conexion.ConexionBD();
            conexionRetorno.Open();
            List<Aeropuerto> colaboradores = new List<Aeropuerto>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, iata, nombre, pais FROM aeropuertos", conexionRetorno);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaboradores.Add(cargar(dr));
                }
            }
            conexionRetorno.Close();
            return colaboradores;
        }

        public Aeropuerto cargar(NpgsqlDataReader reader)
        {
            Aeropuerto aeropuerto = new Aeropuerto
            {
                id = reader.GetInt16(0),
                iata = reader.GetString(1),
                nombre = reader.GetString(2),
                pais = reader.GetString(3)
            };
            return aeropuerto;
        }
    }
}
