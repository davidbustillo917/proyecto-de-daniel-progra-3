﻿using DanielHumbertoVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHumbertoVuelos.Datos
{
    class UsuarioDAO
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();


        public List<Usuario> autenticar(Usuario u)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                List<Usuario> colaboradores = new List<Usuario>();
                NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre, edad, contraseña, tipo FROM usuarios where cedula = @ced and contraseña = @contra", conexionRetorno);
                cmd.Parameters.AddWithValue("@ced", u.cedula);
                cmd.Parameters.AddWithValue("@contra", u.contrasena);

                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        colaboradores.Add(cargar(dr));
                    }
                }
                conexionRetorno.Close();
                return colaboradores;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return null;
            }
        }

        public Usuario cargar(NpgsqlDataReader reader)
        {
            Usuario usuario = new Usuario
            {
                id = reader.GetInt32(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
                edad = reader.GetInt32(3),
                contrasena = reader.GetString(4),
                tipo = reader.GetChar(5)

            };
            return usuario;
        }

        public bool insertar(Usuario u)
        {
            try
            {
                conexionRetorno = conexion.ConexionBD();
                conexionRetorno.Open();
                NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) " +
                    "VALUES ('" + u.cedula+ "', '" + u.nombre + "', '" + u.edad + "', '" + u.contrasena + "', '" + u.tipo + "')", conexionRetorno);
                return cmd.ExecuteNonQuery() == 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //public Aerolinea cargarAerolinea(int id)
        //{
        //    conexionRetorno = conexion.ConexionBD();
        //    conexionRetorno.Open();
        //    NpgsqlCommand cmd = new NpgsqlCommand("SELECT nombre, ano_fundacion, tipo FROM aerolinea where id = '" + id + "'", conexionRetorno);
        //    NpgsqlDataReader dr = cmd.ExecuteReader();
        //    if (dr.HasRows)
        //    {
        //        while (dr.Read())
        //        {
        //            return cargar(dr);
        //        }
        //    }
        //    conexionRetorno.Close();
        //    return null;
        //}
    }
}

