﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmReportes : Form
    {
        VueloBO vbo;
        AerolineaBO aebo;
        TripulacionBO tbo;
        AvionBO abo;
        public FrmReportes()
        {
            InitializeComponent();
            vbo = new VueloBO();
            aebo = new AerolineaBO();
            tbo = new TripulacionBO();
            abo = new AvionBO();
            mostrarAerolineas();
        }

        //private void mostrarVuelosPorFecha()
        //{
        //    DateTime fecha1 = dtpDesde.Value;
        //    DateTime fecha2 = dtpHasta.Value;
        //    foreach(Vuelo item in vbo.cargarPorFecha(fecha1, fecha2))
        //    {
        //        lista1.Rows.Add(item);
        //    }
        //}

        public void mostrarAerolineas()
        {
            foreach (Aerolinea item in aebo.cargar())
            {
                comboAero.Items.Add(item);
                comboAero2.Items.Add(item);
            }
        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DateTime fecha1 = dtpDesde.Value;
            DateTime fecha2 = dtpHasta.Value;
            lista1.Rows.Clear();
            foreach (Vuelo item in vbo.cargarPorFecha(fecha1, fecha2))
            {
                lista1.Rows.Add(item, item.precio, item.fechaSalida, item.aeropuertoSalida, item.fechaLlegada, item.aeropuertoLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aerolinea a = (Aerolinea)comboAero.SelectedItem;
            int id = a.id;
            lista2.Rows.Clear();
            foreach (Vuelo item in vbo.cargarListaDeVuelos(id))
            {
                lista2.Rows.Add(item, item.precio, item.fechaSalida, item.aeropuertoSalida, item.fechaLlegada, item.aeropuertoLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }
        }

        private void btnBuscar2_Click(object sender, EventArgs e)
        {
            Aerolinea a = (Aerolinea)comboAero2.SelectedItem;
            int id = a.id;
            lista4.Rows.Clear();
            foreach (Tripulacion item in tbo.cargarTripulacionPorAerolinea(id))
            {
                lista4.Rows.Add(item, item.cedula, item.nombre, item.fechaNacimiento, item.aerolinea, item.rol, item.estado);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lista3.Rows.Clear();
            foreach (Avion item in abo.cargarAvionesPorRanking())
            {
                lista3.Rows.Add(item, item.modelo, item.annoConstruccion, item.aerolinea, item.capacidad, item.estado);
            }
        }

        private void FrmReportes_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
