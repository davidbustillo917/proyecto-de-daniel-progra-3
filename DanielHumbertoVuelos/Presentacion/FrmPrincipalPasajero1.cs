﻿using DanielHumbertoVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmPrincipalPasajero1 : Form
    {
        Usuario usuario;
        public FrmPrincipalPasajero1(Object u)
        {
            InitializeComponent();
            this.usuario = (Usuario)u;
        }

        private void busquedaYCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPasajero frm = new FrmPasajero(usuario);
            frm.Show();
            this.Hide();
        }

        private void busquedaInteligenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVuelosInteligentes frm = new FrmVuelosInteligentes();
            frm.Show();
            this.Hide();
        }

        private void FrmPrincipalPasajero1_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmLogin frm = new FrmLogin();
            frm.Show();
        }
    }
}
