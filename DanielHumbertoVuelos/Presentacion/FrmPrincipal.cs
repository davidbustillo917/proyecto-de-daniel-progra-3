﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmPrincipal : Form
    {
        //AerolineaBO abo;
        //TripulacionBO tbo;
        //AvionBO avbo;
        //AeropuertoBO aebo;
        //VueloBO vbo;
        public FrmPrincipal()
        {
            InitializeComponent();
            //abo = new AerolineaBO();
            //tbo = new TripulacionBO();
            //avbo = new AvionBO();
            //aebo = new AeropuertoBO();
            //vbo = new VueloBO();
            //cargar1();
            //cargar2();
            //cargar3();
            //cargar4();
            //cargar5();
        }

        //public void cargar1()
        //{
        //    foreach(Aerolinea item in abo.cargar())
        //    {
        //        lista1.Rows.Add(item, item.nombre, item.annoFundacion, item.tipo);
        //    }
        //}

        //public void cargar2()
        //{
        //    foreach (Tripulacion item in tbo.cargar())
        //    {
        //        lista2.Rows.Add(item, item.cedula, item.nombre, item.fechaNacimiento, item.aerolinea, item.rol, item.estado); ;
        //    }
        //}

        //public void cargar3()
        //{
        //    foreach (Avion item in avbo.cargar())
        //    {
        //        lista3.Rows.Add(item, item.modelo, item.annoConstruccion, item.aerolinea, item.capacidad, item.estado); 
        //    }
        //}

        //public void cargar4()
        //{
        //    foreach (Aeropuerto item in aebo.cargar())
        //    {
        //        lista4.Rows.Add(item, item.iata, item.nombre, item.pais);
        //    }
        //}

        //public void cargar5()
        //{
        //    foreach (Vuelo item in vbo.cargar())
        //    {
        //        lista5.Rows.Add(item, item.precio, item.fechaSalida, item.aeropuertoSalida, item.fechaLlegada, item.aeropuertoLlegada, item.aerolinea, item.avion, item.tripulacion, item.horas);
        //    }
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            FrmLogin frm = new FrmLogin();
            frm.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmAerolineaReg frm = new FrmAerolineaReg();
            frm.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FrmTripulaionReg frm = new FrmTripulaionReg();
            frm.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmAvionesReg frm = new FrmAvionesReg();
            frm.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmAeropuertoReg frm = new FrmAeropuertoReg();
            frm.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmVuelosReg frm = new FrmVuelosReg();
            frm.Show();
            this.Hide();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void aerolíneasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmManAerolinea frm = new FrmManAerolinea();
            frm.Show();
            this.Hide();
        }

        private void tripulaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTripulaionReg frm = new FrmTripulaionReg();
            frm.Show();
            this.Hide();
        }

        private void avionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAvionesReg frm = new FrmAvionesReg();
            frm.Show();
            this.Hide();
        }

        private void aeropuesrtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAeropuertoReg frm = new FrmAeropuertoReg();
            frm.Show();
            this.Hide();
        }

        private void vuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmVuelosReg frm = new FrmVuelosReg();
            frm.Show();
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmLogin frm = new FrmLogin();
            frm.Show();
        }

        private void reportesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmReportes frm = new FrmReportes();
            frm.Show();
        }
    }
}
