﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmAvionesReg : Form
    {
        AvionBO abo;
        AerolineaBO aebo;
        public FrmAvionesReg()
        {
            InitializeComponent();
            abo = new AvionBO();
            aebo = new AerolineaBO();
            cargarAerolineas();
            cargar3();
        }

        public void cargarAerolineas()
        {
            foreach (Aerolinea item in aebo.cargar())
            {
                comboAero.Items.Add(item);
            }
        }

        public void cargar3()
        {
            lista3.Rows.Clear();
            foreach (Avion item in abo.cargar())
            {
                lista3.Rows.Add(item, item.modelo, item.annoConstruccion, item.aerolinea, item.capacidad, item.estado);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Avion a = new Avion();
            a.modelo = txtModelo.Text;
            a.annoConstruccion = dtpAnno.Value;
            a.aerolinea = (Aerolinea)comboAero.SelectedItem;
            a.capacidad = int.Parse(txtCapacidad.Text);
            a.estado = rbDisponible.Checked ? 'D' : 'U';

            if (abo.registrar(a))
            {
                MessageBox.Show("Avion registrado");
                cargar3();
                //this.Close();
            }
        }

        private void FrmAvionesReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
