﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmAeropuertoReg : Form
    {
        AeropuertoBO abo;
        public FrmAeropuertoReg()
        {
            InitializeComponent();
            abo = new AeropuertoBO();
            cargar4();
        }

        public void cargar4()
        {
            lista4.Rows.Clear();
            foreach (Aeropuerto item in abo.cargar())
            {
                lista4.Rows.Add(item, item.iata, item.nombre, item.pais);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aeropuerto a = new Aeropuerto();
            a.iata = txtIata.Text;
            a.nombre = txtNombre.Text;
            a.pais = txtPais.Text;

            if (abo.registrar(a))
            {
                MessageBox.Show("Aeropuerto registrado");
                cargar4();
            }
        }

        private void FrmAeropuertoReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
