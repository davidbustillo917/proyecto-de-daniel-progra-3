﻿using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmPasajero : Form
    {
        AeropuertoBO apbo;
        VueloBO vbo;
        List<Vuelo> Escala1;
        List<Vuelo> Escala2;
        List<Vuelo> vueloEscala;
        Usuario usuario;
        Vuelo v;
        Vuelo vt;
        String escalaHistoria;
        double hora;
        double precio;
        public FrmPasajero(object u)
        {
            InitializeComponent();
            apbo = new AeropuertoBO();
            vbo = new VueloBO();
            v = new Vuelo();
            vt = new Vuelo();
            cargarAeropuertos();
            cargarVuelos();
            Escala1 = new List<Vuelo>();
            Escala2 = new List<Vuelo>();
            vueloEscala = new List<Vuelo>();
            this.usuario = (Usuario)u;
            escalaHistoria = " ";
            hora = 0;
            precio = 0;
            //MessageBox.Show(this.usuario.ToString());
            //CargarEscalas();
        }

        public void cargarAeropuertos()
        {
            //foreach (Aeropuerto item in apbo.cargar())
            //{
            //    cbxOrigen.Items.Add(item);
            //    cbxDestino.Items.Add(item);
            //}
            cbxOrigen.DataSource = apbo.cargar();
            cbxDestino.DataSource = apbo.cargar();
        }

        public void cargarVuelos()
        {
            foreach(Vuelo item in vbo.cargar())
            {
                dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, "   ", item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1, item.horas);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Retorno_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void dtgVuelos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            DateTime fecha1 = dtpSalida.Value;
            DateTime fecha2 = dtpLlegada.Value;
            string aero1 = cbxOrigen.Text;
            string aero2 = cbxDestino.Text;


            dtgVuelos.Rows.Clear();
            foreach (Vuelo item1 in vbo.cargar())
            {
                if (item1.aeropuertoLlegada != aero2)
                {
                    CargarEscalas(aero1, aero2);
                    
                }
                else if (item1.aeropuertoLlegada == aero2)
                {

                    foreach (Vuelo item in vbo.cargarPorBusqueda(fecha1, fecha2, aero1, aero2))
                    //foreach (Vuelo esca in vueloEscala)
                    {
                        dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, " ", item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
                        ////break;
                    }
                }
                //break;
            }
            //Escala1.Clear();
            //Escala2.Clear();

            //if (vueloEscala.Count != null)
            //{

            //MessageBox.Show("Listas limpiadas");
            //}
            //crearEscalas(aero2, aero1);

            //foreach (Vuelo item in vbo.cargarPorBusqueda(fecha1, fecha2, aero1, aero2))
            //    //foreach (Vuelo esca in vueloEscala)
            //    {
            //        dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, item, item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
            //    }


        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dtgVuelos.Rows.Clear();
            foreach(Vuelo item in vbo.cargarPorPrecio())
            {
                dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, " ", item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }

        }
////////////////////////////////////   Metodo para hacer las escalas  //////////////////////////////////////////////////////////////////////////
        private void cbxDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            string salida = cbxOrigen.Text;
            string destino = cbxDestino.Text;
           
        }

        

        public void CargarEscalas(string salida, string destino)
        {
            foreach (Vuelo item in vbo.cargar())
            {
                if (salida == item.aeropuertoSalida)
                {
                    Escala1.Add(item);
                }
            }
            

            //MessageBox.Show(e);
            foreach (Vuelo item in vbo.cargar())
            {
                if (destino == item.aeropuertoLlegada)
                {
                    Escala2.Add(item);
                }
            }

            
            foreach (Vuelo item1 in Escala1)
                foreach (Vuelo item2 in Escala2)
                {
                    if (item1.aeropuertoLlegada == item2.aeropuertoSalida)
                    {
                        dtgVuelos.Rows.Add(item1, item1.id, item1.precio, item1.fechaSalida, item1.aeropuertoSalida, item1.aeropuertoLlegada, item2.aeropuertoLlegada, item1.fechaLlegada, item1.aerolinea, item1.avion, item1.tripulacion1);
                        escalaHistoria = item2.aeropuertoLlegada;
                        hora = item2.horas;
                        precio = item2.precio;
                        //MessageBox.Show(item1.aeropuertoSalida + " // " + item1.aeropuertoLlegada + " /// " + item2.aeropuertoSalida + " // " + item2.aeropuertoLlegada);
                    }
                    break;
                    
                }
            Escala1.Clear();
            Escala2.Clear();
            {

            }
        }

        private void FrmPasajero_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipalPasajero1 frm = new FrmPrincipalPasajero1(usuario);
            frm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dtgVuelos.Rows.Clear();
            foreach (Vuelo item in vbo.cargarPorPrecio())
            {
                dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, " ", item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }
    }

        private void button2_Click(object sender, EventArgs e)
        {
            Historial h = new Historial();

            v = (Vuelo)dtgVuelos.CurrentRow.Cells[0].Value;

            h.cedula = usuario.cedula;
            h.paisSalida = v.aeropuertoSalida;
            h.paisLlegada = v.aeropuertoLlegada;
            h.paisEscala2 = null;
            h.fechaCompra = DateTime.Now;
            MessageBox.Show(hora + " / " + precio);
            double sumaHoras = v.horas + hora;
            double sumaPrecios = v.precio + precio;

            h.horas = sumaHoras;
            h.costoTotal = sumaPrecios;
            h.paisEscala1 = escalaHistoria;
            if (vbo.registrarHistorial(h))
            {
                MessageBox.Show("Compra registrada");
                this.Close();
            }
        }
    }
}
