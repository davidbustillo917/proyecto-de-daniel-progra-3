﻿using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmLogin : Form
    {
        UsuarioBO ubo;
        Usuario u;


        public FrmLogin()
        {
            InitializeComponent();
            ubo = new UsuarioBO();
            u = new Usuario();
        }

        public static string Encriptar(string cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }
        private void btnIniciar_Click(object sender, EventArgs e)
        {
            u.cedula = txtUsuario.Text;
            u.contrasena = txtContrasena.Text;
            u.contrasena = Encriptar(txtContrasena.Text);
            List<Usuario> Lista = ubo.cargar(u);

            for (int i = 0; i < Lista.Count; i++)
            {
                if (Lista[i].tipo.Equals('A'))
                {
                    MessageBox.Show("Hola " + Lista[i].nombre);
                    FrmPrincipal frm = new FrmPrincipal();
                    frm.Show();
                    this.Hide();
                }
                else if (Lista[i].tipo.Equals('P'))
                {
                    Usuario usu = Lista[i];
                    MessageBox.Show("Hola Pasajero " + Lista[i].nombre);
                    FrmPrincipalPasajero1 frm = new FrmPrincipalPasajero1(usu);
                    frm.Show();
                    this.Hide();
                }
                else if (Lista[i].tipo.Equals('T'))
                {
                    MessageBox.Show("Hola tripulante " + Lista[i].nombre);
                }

            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            FrmUsuarioReg frm = new FrmUsuarioReg();
            frm.Show();
            this.Hide();
        }

        private void FrmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtContrasena_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
