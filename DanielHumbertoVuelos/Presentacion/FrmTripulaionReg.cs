﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmTripulaionReg : Form
    {
        TripulacionBO tbo;
        AerolineaBO abo;
        public FrmTripulaionReg()
        {
            InitializeComponent();
            tbo = new TripulacionBO();
            abo = new AerolineaBO();
            cargarAerolineas();
            cargar2();
        }

        public void cargarAerolineas()
        {
            foreach(Aerolinea item in abo.cargar())
            {
                cbAerolinea.Items.Add(item);
            }
        }

        public void cargar2()
        {
            lista2.Rows.Clear();
            foreach (Tripulacion item in tbo.cargar())
            {
                lista2.Rows.Add(item, item.cedula, item.nombre, item.fechaNacimiento, item.aerolinea, item.rol, item.estado);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tripulacion t = new Tripulacion();
            t.cedula = txtCedula.Text;
            t.nombre = txtNombre.Text;
            t.fechaNacimiento = dtpFechaNa.Value;
            t.aerolinea = (Aerolinea)cbAerolinea.SelectedItem;
            t.rol = rbPiloto.Checked ? 'P' : 'S';
            t.estado = chxDisponible.Checked ? 'D' : 'S';

            if (tbo.registrar(t))
            {
                MessageBox.Show("Tripulaion registrada");
                cargar2();
                this.Close();
            }
        }

        private void FrmTripulaionReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
