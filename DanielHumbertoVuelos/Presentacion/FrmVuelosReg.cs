﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmVuelosReg : Form
    {
        AeropuertoBO apbo;
        AerolineaBO aebo;
        AvionBO abo;
        TripulacionBO tbo;
        VueloBO vbo;
   
        public FrmVuelosReg()
        {
            InitializeComponent();
            aebo = new AerolineaBO();
            abo = new AvionBO();
            tbo = new TripulacionBO();
            apbo = new AeropuertoBO();
            vbo = new VueloBO();
            //asignarAvion();
            cargarAerolineas();
            //cargarAviones();
            //cargarTripulacion();
            cargarAeropuertos();
            cargar5();
        }
        public void cargarAerolineas()
        {
            foreach (Aerolinea item in aebo.cargar())
            {
                comboAerolinea.Items.Add(item);
            }
        }

        public void cargar5()
        {
            lista5.Rows.Clear();
            foreach (Vuelo item in vbo.cargar())
            {
                lista5.Rows.Add(item, item.precio, item.fechaSalida, item.aeropuertoSalida, item.fechaLlegada, item.aeropuertoLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }
        }

        public void cargarAeropuertos()
        {
            foreach (Aeropuerto item in apbo.cargar())
            {
                comboAeropuertoLlegada.Items.Add(item);
                comboAeropuertoSalida.Items.Add(item);
            }
        }

        public void cargarAviones()
        {
            //int num = ran.Next(1, (abo.cargar().Count));
            //Aerolinea aerolinea = (Aerolinea)comboAerolinea.SelectedItem;
            //int idAerolinea = aerolinea.id;

            //Avion item = abo.cargarAvionPorAerolinea(num, idAerolinea);
            //MessageBox.Show(item.ToString());
            //comboAvion.Items.Add(item);
        }

        Random ran = new Random();

        //public void asignarAvion()
        //{
        //    int num = ran.Next(abo.cargar().Count);
        //    MessageBox.Show(num.ToString());

        //    //for(int i = 0; i < abo.cargar().Count; i++)
        //    //{
        //    //    MessageBox.Show(i.ToString());
        //    //}
        //}

        public void cargarTripulacion()
        {
            //Aerolinea a = (Aerolinea)comboAerolinea.SelectedItem;
            //int id = a.id;
            //for (int i = 0; i < 2; i++)
            //{
            //    foreach (Tripulacion item in tbo.cargarPilotos(id))
            //    {
            //        txtTripulante1.Text = item.ToString();
            //        txtTripulante2.Text = item.ToString();
            //    }
            //}
        }
        private void FrmVuelosReg_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vuelo v = new Vuelo();
            int idPrueba1 = 0;
            int idPrueba2 = 0;
            int idPrueba3 = 0;
            int idPrueba4 = 0;
            int idPrueba5 = 0;

            Aerolinea a = (Aerolinea)comboAerolinea.SelectedItem;
            int id = a.id;
            foreach (Tripulacion t1 in tbo.cargarPilotos(id))
            {
                foreach (Tripulacion t2 in tbo.cargarPilotos(id))
                {
                    if (t1.id != t2.id)
                    {
                        txtTripulante2.Text = t2.ToString();
                        v.tripulacion2 = t2;
                        idPrueba2 = t2.id;
                    }
                }
                txtTripulante1.Text = t1.ToString();
                v.tripulacion1 = t1;
                idPrueba1 = t1.id;
            }



            foreach (Tripulacion t3 in tbo.cargarServicio(id))
            {
                foreach (Tripulacion t4 in tbo.cargarServicio(id))
                {
                    if (t3.id != t4.id)
                    {
                        txtTripulante4.Text = t4.ToString();
                        v.tripulacion4 = t4;
                        idPrueba4 = t4.id;
                    }
                    foreach (Tripulacion t5 in tbo.cargarServicio(id))
                    {
                        if (t3.id != t5.id && t4.id != t5.id && t3.id != t4.id)
                        {
                            txtTripulante5.Text = t5.ToString();
                            v.tripulacion5 = t5;
                            idPrueba5 = t5.id;
                        }
                    }
                }
                txtTripulante3.Text = t3.ToString();
                v.tripulacion3 = t3;
                idPrueba3 = t3.id;
            }

            v.precio = double.Parse(txtPrecio.Text);
            v.fechaSalida = dtpFechSalida.Value;
            v.aeropuertoSalida = comboAeropuertoSalida.Text;
            v.fechaLlegada = dtpFechaLlegada.Value;
            v.aeropuertoLlegada = comboAeropuertoLlegada.Text;
            v.aerolinea = (Aerolinea)comboAerolinea.SelectedItem;
            v.avion = (Avion)comboAvion.SelectedItem;
            v.horas = double.Parse(txtHoras.Text);
            
            int estado = v.avion.id;
                if (vbo.registrar(v))
                {
                    tbo.cambiarEstado(idPrueba1);
                    tbo.cambiarEstado(idPrueba2);
                    tbo.cambiarEstado(idPrueba3);
                    tbo.cambiarEstado(idPrueba4);
                    tbo.cambiarEstado(idPrueba5);
                    abo.cambiarEstadoAvion(estado);
                    MessageBox.Show("Vuelo Registrado");
                    cargar5();

                }
            
           
            
        }

        private void FrmVuelosReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }

        private void txtHoras_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void comboAerolinea_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Aerolinea aerolinea = (Aerolinea)comboAerolinea.SelectedItem;
                int idAerolinea = aerolinea.id;
                int num = ran.Next(1, (abo.cargarParaRandom(idAerolinea).Count));
                MessageBox.Show(num.ToString());
                MessageBox.Show(idAerolinea.ToString());

                //foreach (Avion item in abo.cargarPorAerolinea(num, idAerolinea))
                //{
                //    comboAvion.Items.Add(item);
                //}

                foreach (Avion item in abo.cargarParaRandom(idAerolinea))
                {
                    comboAvion.Items.Clear();
                    comboAvion.Items.Add(item);
                }
            }
            catch
            {
                MessageBox.Show("Tiene que ingresar mas aviones");
            }


            //    Aerolinea a = (Aerolinea)comboAerolinea.SelectedItem;
            //    int id = a.id;
            //    foreach (Tripulacion t1 in tbo.cargarPilotos(id))
            //    {
            //        foreach (Tripulacion t2 in tbo.cargarPilotos(id))
            //        {
            //            if (t1.id != t2.id)
            //            {
            //                txtTripulante2.Text = t2.ToString();
            //                v.tripulacion2 = t2;
            //                idPrueba2 = t2.id;
            //            }
            //        }
            //        txtTripulante1.Text = t1.ToString();
            //        v.tripulacion1 = t1;
            //        idPrueba1 = t1.id;
            //    }

            //    foreach (Tripulacion t3 in tbo.cargarServicio(id))
            //    {
            //        foreach (Tripulacion t4 in tbo.cargarServicio(id))
            //        {
            //            if (t3.id != t4.id)
            //            {
            //                txtTripulante4.Text = t4.ToString();
            //                v.tripulacion4 = t4;
            //                idPrueba4 = t4.id;
            //            }
            //            foreach (Tripulacion t5 in tbo.cargarServicio(id))
            //            {
            //                if (t3.id != t5.id && t4.id != t5.id && t3.id != t4.id)
            //                {
            //                    txtTripulante5.Text = t5.ToString();
            //                    v.tripulacion5 = t5;
            //                    idPrueba5 = t5.id;
            //                }
            //            }
            //        }
            //        txtTripulante3.Text = t3.ToString();
            //        v.tripulacion3 = t3;
            //        idPrueba3 = t3.id;
            //    }
        }
    }
}
