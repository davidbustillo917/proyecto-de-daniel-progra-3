﻿using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmManAerolinea : Form
    {
        AerolineaBO abo;
        public FrmManAerolinea()
        {
            InitializeComponent();
            abo = new AerolineaBO();
            cargar1();
        }

        private void lista5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void cargar1()
        {
            lista1.Rows.Clear();
            foreach (Aerolinea item in abo.cargar())
            {
                lista1.Rows.Add(item, item.nombre, item.annoFundacion, item.tipo);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aerolinea a = new Aerolinea();
            a.nombre = txtNombre.Text;
            a.annoFundacion = dtpAnno.Value;
            a.tipo = rbInter.Checked ? 'I' : 'L';

            if (abo.registrar(a))
            {
                MessageBox.Show("Aerolinea registrada");
                Refresh();
                cargar1();
            }
        }

        private void FrmManAerolinea_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
