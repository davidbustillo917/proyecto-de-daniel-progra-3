﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmVuelosInteligentes : Form
    {
        AeropuertoBO apbo;
        VueloBO vbo;
        public FrmVuelosInteligentes()
        {
            InitializeComponent();
            apbo = new AeropuertoBO();
            vbo = new VueloBO();
            cargarAeropuertos();
            cargarVuelos();
        }

        public void cargarAeropuertos()
        {
            foreach (Aeropuerto item in apbo.cargar())
            {
                cbxOrigen.Items.Add(item);
                cbxDestino.Items.Add(item);
            }
        }

        public void cargarVuelos()
        {
            foreach (Vuelo item in vbo.cargar())
            {
                dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, "   ", item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1, item.horas);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dtgVuelos.Rows.Clear();
            DateTime fecha1 = dtpSalida.Value;
            DateTime fecha2 = dtpLlegada.Value;
            string aero1 = cbxOrigen.Text;
            string aero2 = cbxDestino.Text;


            //crearEscalas(aero2, aero1);
            if (txtRangoDias.Text == "")
            {
                foreach (Vuelo item in vbo.cargarPorBusqueda(fecha1, fecha2, aero1, aero2))
                {
                    dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, item, item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
                }
            }
            else
            {
                int dias = int.Parse(txtRangoDias.Text);
                MessageBox.Show("Va bien");
                DateTime fechaSalida1 = fecha1.AddDays(-dias);
                MessageBox.Show(fechaSalida1.ToString());
                DateTime fechaSalida2 = fecha1.AddDays(dias);
                MessageBox.Show(fechaSalida2.ToString());
                DateTime fechaLlegada1 = fecha2.AddDays(-dias);
                MessageBox.Show(fechaLlegada1.ToString());
                DateTime fechaLlegada2 = fecha2.AddDays(dias);
                MessageBox.Show(fechaLlegada2.ToString());
                foreach (Vuelo item in vbo.cargarPorBusquedaInteligente(aero1, aero2, fechaSalida1, fechaSalida2, fechaLlegada1, fechaLlegada2))
                {
                    dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, item, item.aeropuertoLlegada, item.fechaLlegada, item.aerolinea, item.avion, item.tripulacion1);
                }
                ;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dtgVuelos.Rows.Clear();
            foreach (Vuelo item in vbo.cargarPorPrecio())
            {
                dtgVuelos.Rows.Add(item, item.id, item.precio, item.fechaSalida, item.aeropuertoSalida, item.fechaLlegada, item.aeropuertoLlegada, item.aerolinea, item.avion, item.tripulacion1);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void FrmVuelosInteligentes_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipalPasajero1 frm = new FrmPrincipalPasajero1(null);
            frm.Show();
        }
    }
}
