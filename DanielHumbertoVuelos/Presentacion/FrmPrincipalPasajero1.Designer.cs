﻿namespace DanielHumbertoVuelos.Presentacion
{
    partial class FrmPrincipalPasajero1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buquedasDeVuelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busquedaYCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busquedaInteligenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buquedasDeVuelosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buquedasDeVuelosToolStripMenuItem
            // 
            this.buquedasDeVuelosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.busquedaYCompraToolStripMenuItem,
            this.busquedaInteligenteToolStripMenuItem});
            this.buquedasDeVuelosToolStripMenuItem.Name = "buquedasDeVuelosToolStripMenuItem";
            this.buquedasDeVuelosToolStripMenuItem.Size = new System.Drawing.Size(124, 20);
            this.buquedasDeVuelosToolStripMenuItem.Text = "Buquedas de vuelos";
            // 
            // busquedaYCompraToolStripMenuItem
            // 
            this.busquedaYCompraToolStripMenuItem.Name = "busquedaYCompraToolStripMenuItem";
            this.busquedaYCompraToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.busquedaYCompraToolStripMenuItem.Text = "Busqueda y compra";
            this.busquedaYCompraToolStripMenuItem.Click += new System.EventHandler(this.busquedaYCompraToolStripMenuItem_Click);
            // 
            // busquedaInteligenteToolStripMenuItem
            // 
            this.busquedaInteligenteToolStripMenuItem.Name = "busquedaInteligenteToolStripMenuItem";
            this.busquedaInteligenteToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.busquedaInteligenteToolStripMenuItem.Text = "Busqueda Inteligente";
            this.busquedaInteligenteToolStripMenuItem.Click += new System.EventHandler(this.busquedaInteligenteToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel1.BackgroundImage = global::DanielHumbertoVuelos.Properties.Resources.comparador_vuelos;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-79, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(926, 465);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(365, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Menu Principal";
            // 
            // FrmPrincipalPasajero1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipalPasajero1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pasajero";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipalPasajero1_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem buquedasDeVuelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busquedaYCompraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busquedaInteligenteToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
    }
}