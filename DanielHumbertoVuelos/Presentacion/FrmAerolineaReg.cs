﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmAerolineaReg : Form
    {
        AerolineaBO abo;
        public FrmAerolineaReg()
        {
            InitializeComponent();
            abo = new AerolineaBO();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aerolinea a = new Aerolinea();
            a.nombre = txtNombre.Text;
            a.annoFundacion = dtpAnno.Value;
            a.tipo = rbInter.Checked ? 'I' : 'L';

            if (abo.registrar(a))
            {
                MessageBox.Show("Aerolinea registrada");
                this.Close();
            }
        }

        private void FrmAerolineaReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmPrincipal frm = new FrmPrincipal();
            frm.Show();
        }
    }
}
