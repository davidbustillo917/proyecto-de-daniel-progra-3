﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Entidades;
using DanielHumbertoVuelos.Negocio;

namespace DanielHumbertoVuelos.Presentacion
{
    public partial class FrmUsuarioReg : Form
    {
        UsuarioBO ubo;
        public FrmUsuarioReg()
        {
            InitializeComponent();
            ubo = new UsuarioBO();
        }

        public static string Encriptar(string cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Usuario u = new Usuario();
            u.cedula = txtCedula.Text;
            u.nombre = txtNombre.Text;
            u.edad = int.Parse(txtEdad.Text);
            u.contrasena = Encriptar(txtContra.Text);
            u.tipo = rbPasajero.Checked ? 'P' : 'A';

            if (ubo.registrar(u))
            {
                MessageBox.Show("Usuario registrado");
                this.Close();
            }
        }

        private void FrmUsuarioReg_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FrmLogin frm = new FrmLogin();
            frm.Show();
        }
    }
}
