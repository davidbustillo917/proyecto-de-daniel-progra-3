CREATE TABLE usuarios(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	edad int,
	contraseña text not null,
	tipo char not null
)


CREATE TABLE aerolinea(
	id serial primary key,
	nombre text not null,
	ano_fundacion Date not null,
	tipo char not null
)

CREATE TABLE tripulacion(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	fecha_nacimiento Date not null,
	id_aerolinea int not null,
	rol char not null,
	estado char not null,
	CONSTRAINT fk_aero_trip foreign key (id_aerolinea) references aerolinea (id),
	CONSTRAINT unq_cedtrip_cedula UNIQUE (cedula)
)





CREATE TABLE aviones(	
	id serial primary key,
	modelo text not null,
	ano_construccion Date not null,
	id_aerolinea int not null,
	capacidad int not null,
	estado char default 'D' 
)
select * from aviones
ALTER TABLE aviones ADD CONSTRAINT fk_aero_avi foreign key (id_aerolinea) references aerolinea (id)




CREATE TABLE aeropuertos(
	id serial primary key,
	iata text not null,
	nombre text not null,
	pais text not null
)


CREATE TABLE vuelos(
	id serial primary key,
	precio numeric not null,
	fecha_salida Date not null,
	aeropuerto_salida text not null,
	fecha_llegada Date not null,
	aeropuerto_llegada text not null,
	id_aerolinea int not null,
	id_avion int not null,
	id_tripulacion int not null,
	id_tripulacion2 int not null,
	id_tripulacion3 int not null,
	id_tripulacion4 int not null,
	id_tripulacion5 int not null,
	horas int not null,
	CONSTRAINT fk_aero_vue foreign key (id_aerolinea) references aerolinea (id),
	CONSTRAINT fk_avi_vue foreign key (id_avion) references aviones (id),
	CONSTRAINT fk_trip_vue foreign key (id_tripulacion) references tripulacion (id)
)




CREATE TABLE historial(
	id serial primary key,
	cedula text not null,
	nombre_pais_salida text not null,
	nombre_pais_llegada text not null,
	nombre_pais_escala1 text not null,
	nombre_pais_escala2 text not null,
	fecha_ompra Date not null,
	tiempo_vuelo numeric not null,
	costo numeric not null	
)
drop table historial
select * from historial

CREATE TABLE historial(
	id serial primary key,
	cedula text not null,
	pais_salida text not null,
	pais_llegada text not null,
	pais_escala1 text not null,
	pais_escala2 text not null,
	fecha_compra Date not null,
	Horas numeric not null,
	costo numeric not null
)








--Usuario
INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) VALUES ('208220031', 'Daniel Alfaro', 18, 'dell2020', 'A')
INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) VALUES ('00000000', 'Geberth Alfaro', 18, 'gdas6462', 'P')
INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) VALUES ('111', 'Humberto Vargas', 21, 'humberto', 'T')
INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) VALUES ('222', 'Maria Cedeñp', 20, 'humberto', 'T')
INSERT INTO usuarios (cedula, nombre, edad, contraseña, tipo) VALUES ('333', 'Juan Cordero', 19, 'humberto', 'T')

--Aerolinea
INSERT INTO aerolinea (nombre, ano_fundacion, tipo) VALUES ('Rakuten','2019-08-04', 'I')
INSERT INTO aerolinea (nombre, ano_fundacion, tipo) VALUES ('Avianca','2018-03-02', 'I')
INSERT INTO aerolinea (nombre, ano_fundacion, tipo) VALUES ('Airlanes','2017-10-01', 'L')
INSERT INTO aerolinea (nombre, ano_fundacion, tipo) VALUES ('Las nubes','2020-01-01', 'L')
INSERT INTO aerolinea (nombre, ano_fundacion, tipo) VALUES ('Skyrecord','2019-12-11', 'I')

--Aeropuerto
INSERT INTO aeropuertos (iata, nombre, pais) VALUES ('SJO', 'Aeropuerto JuanSantamaria', 'Costa Rica')
INSERT INTO aeropuertos (iata, nombre, pais) VALUES ('CUN', 'Aeropuerto Internacional de Cancun', 'Mexico')
INSERT INTO aeropuertos (iata, nombre, pais) VALUES ('YVR', 'Aeropuerto Internacional de Vancouver', 'Canada')
INSERT INTO aeropuertos (iata, nombre, pais) VALUES ('BOG', 'Aeropuerto Internacional el Dorado', 'Colombia')
INSERT INTO aeropuertos (iata, nombre, pais) VALUES ('EZE', 'Aeropuerto Internacional de Ezeiza', 'Argentina')

--Tripulacion
INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) VALUES 
('208160251', 'Humberto', '2001-08-04', '1', 'P', 'D')

INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) VALUES 
('29890', 'Daniel', '2002-07-04', '1', 'P', 'D')

INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) VALUES 
('2685', 'Priscilla', '2001-12-12', '1', 'S', 'D')

INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) VALUES 
('87684', 'Noelia', '1998-08-02', '1', 'S', 'D')

INSERT INTO tripulacion (cedula, nombre, fecha_nacimiento, id_aerolinea, rol, estado) VALUES 
('20198', 'Carla', '2003-09-04', '1', 'S', 'D')

--Aviones
INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) VALUES ('AF-3', '2013-05-07', '1', '250', 'D')
INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) VALUES ('ABC-123', '2012-05-07', '1', '300', 'D')
INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) VALUES ('QWE-3', '2018-02-03', '1', '500', 'D')
INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) VALUES ('ZXC-10', '2015-02-10', '1', '350', 'D')
INSERT INTO aviones (modelo, ano_construccion, id_aerolinea, capacidad, estado) VALUES ('ASD-987', '2010-01-01', '1', '200', 'D')

--Vuelos
INSERT INTO vuelos (precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion,
id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas)
VALUES ('1000', '2020-05-02', 'Aeropuerto Internacional de Cancun', '2020-06-03', 'Aeropuerto Internacional de Vancouver', '1', '1', '1', '2', '3', '4', '5', '8')

INSERT INTO vuelos (precio, fecha_salida, aeropuerto_salida, fecha_llegada, aeropuerto_llegada, id_aerolinea, id_avion, id_tripulacion,
id_tripulacion2, id_tripulacion3, id_tripulacion4, id_tripulacion5, horas)
VALUES ('1000', '2020-08-03', 'Aeropuerto Internacional de Cancun', '2020-08-04', 'Aeropuerto Internacional de Vancouver', '1', '1', '1', '2', '3', '4', '5', '8')

--SI DESEA, CREA MAS VUELOS DESDE LA APLICACION PORQUE DESDE AQUI SE NOS HACE DIFICIL POR LOS ID DE LA TRIPULACION























































































