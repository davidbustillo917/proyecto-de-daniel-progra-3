﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;

namespace DanielHumbertoVuelos.Negocio
{
    class AerolineaBO
    {

        public bool registrar(Aerolinea aero)
        {
            if (aero == null)
            {
                throw new RuntimeBinderException("Debe crear una nueva aerolinea");
            }
            if (aero.nombre == null)
            {
                throw new RuntimeBinderException("Nombre requerido");
            }
            if (aero.annoFundacion == null)
            {
                throw new RuntimeBinderException("Año de fundación requerido");
            }
            if (aero.tipo == null)
            {
                throw new RuntimeBinderException("Tipo requerido");
            }
            if (aero.id <= 0)
            {
                return new AerolineaDAO().insertar(aero);
            }
            else
            {
                //return new ColaboradorDAO().editar(cola);
                return false;
            }
        }

        public List<Aerolinea> cargar()
        {
            return new AerolineaDAO().seleccionar();
        }
    }
}
