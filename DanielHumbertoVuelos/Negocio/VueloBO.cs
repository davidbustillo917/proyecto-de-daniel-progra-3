﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;

namespace DanielHumbertoVuelos.Negocio
{
    class VueloBO
    {
        public bool registrar(Vuelo v)
        {
            try
            {
                if (v == null)
                {
                    throw new RuntimeBinderException("Debe crear un nuevo vuelo");
                }
                if (v.precio == 0)
                {
                    throw new RuntimeBinderException("Precio requerido");
                }
                if (v.fechaSalida == null)
                {
                    throw new RuntimeBinderException("Fecha de salida requerida");
                }
                if (v.aeropuertoSalida == null)
                {
                    throw new RuntimeBinderException("Aeropuerto de saida requerido");
                }
                if (v.fechaLlegada == null)
                {
                    throw new RuntimeBinderException("Fecha de llegada requerida");
                }
                if (v.aeropuertoLlegada == null)
                {
                    throw new RuntimeBinderException("Aeropuerto de llegada requerido");
                }
                if (v.aerolinea == null)
                {
                    throw new RuntimeBinderException("Aerolinea requerida");
                }
                if (v.avion == null)
                {
                    throw new RuntimeBinderException("Avión requerido");
                }
                if (v.tripulacion1 == null || v.tripulacion2 == null || v.tripulacion3 == null || v.tripulacion4 == null || v.tripulacion5 == null)
                {
                    throw new RuntimeBinderException("Tripulantes  requerido");
                }
                if (v.id <= 0)
                {
                    return new VueloDAO().insertar(v);
                }
                else
                {
                    return false;
                }
            }
            catch 
            {
                MessageBox.Show("No hay tripulantes disponibles. Si desea registrar más vaya al Registro de tripulantes");
                return false;
            }
        }

        public bool registrarHistorial(Historial h)
        {
            try
            {
                if (h == null)
                {
                    throw new RuntimeBinderException("Debe crear un nuevo historial");
                }
                if (h.paisSalida == null)
                {
                    throw new RuntimeBinderException("Pais de salida requerido");
                }
                if (h.paisLlegada == null)
                {
                    throw new RuntimeBinderException("Pais de llegada requerido");
                }
                if (h.fechaCompra == null)
                {
                    throw new RuntimeBinderException("Fecha de compra requerida");
                }
                if (h.horas == null)
                {
                    throw new RuntimeBinderException("Las horas son requeridas");
                }
                if (h.costoTotal == null)
                {
                    throw new RuntimeBinderException("Costo requerido");
                }
                if (h.id <= 0)
                {
                    return new VueloDAO().insertarHistorial(h);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                MessageBox.Show("Error. Favor intente nuevamente");
                return false;
            }
        }

        public List<Vuelo> cargar()
        {
            return new VueloDAO().seleccionar();
        }

        public List<Vuelo> cargarPorPrecio()
        {
            return new VueloDAO().seleccionarPorPrecio();
        }
        public List<Vuelo> cargarPorRapidez()
        {
            return new VueloDAO().seleccionarPorRapidez();
        }
        public List<Vuelo> cargarPorBusqueda(DateTime fecha1, DateTime fecha2, string aero1, string aero2)
        {
            return new VueloDAO().cargarPorBusqueda(fecha1, fecha2, aero1, aero2);
        }

        public List<Vuelo> cargarPorBusquedaInteligente(string aero1, string aero2, DateTime fechaSalida1, DateTime fechaSalida2, DateTime fechaLlegada1, DateTime fechaLlegada2)
        {
            return new VueloDAO().cargarPorBusquedaInteligente(aero1, aero2, fechaSalida1, fechaSalida2, fechaLlegada1, fechaLlegada2);
        }
        public List<Vuelo> cargarPorFecha(DateTime fecha1, DateTime fecha2)
        {
            return new VueloDAO().vuelosPorFecha(fecha1, fecha2);
        }

        public List<Vuelo> cargarListaDeVuelos(int id)
        {
            return new VueloDAO().vuelosPorAerolinea(id);
        }

        public List<Vuelo> cargarEscalas(string des)
        {
            return new VueloDAO().escalas(des);
        }
    }
}
