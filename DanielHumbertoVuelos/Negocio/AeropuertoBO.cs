﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;

namespace DanielHumbertoVuelos.Negocio
{
    class AeropuertoBO
    {

        public bool registrar(Aeropuerto a)
        {
            if (a == null)
            {
                throw new RuntimeBinderException("Debe crear un nuevo aeropuerto");
            }
            if (a.iata== null)
            {
                throw new RuntimeBinderException("IATA requerido");
            }
            if (a.nombre== null)
            {
                throw new RuntimeBinderException("Nombre requerido");
            }
            if (a.pais == null)
            {
                throw new RuntimeBinderException("País requerido");
            }
            if (a.id <= 0)
            {
                return new AeropuertoDAO().insertar(a);
            }
            else
            {
                //return new ColaboradorDAO().editar(cola);
                return false;
            }
        }

        public List<Aeropuerto> cargar()
        {
            return new AeropuertoDAO().seleccionar();
        }
    }
}
