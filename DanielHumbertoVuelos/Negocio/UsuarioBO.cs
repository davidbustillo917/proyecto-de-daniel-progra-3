﻿using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DanielHumbertoVuelos.Negocio
{
    class UsuarioBO
    {
        UsuarioDAO udao = new UsuarioDAO();

        public List<Usuario> cargar(Usuario u)
        {
            if (u.cedula == null)
            {
                throw new RuntimeBinderException("Usuario requerido");
            }
            if (u.contrasena == null)
            {
                throw new RuntimeBinderException("Contraseña requerida");
            }
           

            return udao.autenticar(u); 
        }

        public bool registrar(Usuario usu)
        {
            if (usu == null)
            {
                throw new RuntimeBinderException("Debe crear un nuevo usuario");
            }
            if (usu.cedula == null)
            {
                throw new RuntimeBinderException("Cédula requerida");
            }
            if (usu.nombre == null)
            {
                throw new RuntimeBinderException("Nombre requerido");
            }
            if (usu.edad == 0)
            {
                throw new RuntimeBinderException("Edad requerida");
            }
            if (usu.tipo == null)
            {
                throw new RuntimeBinderException("Tipo requerido");
            }
            if (usu.id <= 0)
            {
                return new UsuarioDAO().insertar(usu);
            }
            else
            {
                return false;
            }
        }
    }
}
