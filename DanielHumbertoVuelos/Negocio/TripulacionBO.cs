﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;

namespace DanielHumbertoVuelos.Negocio
{
    class TripulacionBO
    {

        public bool registrar(Tripulacion t)
        {
            if (t == null)
            {
                throw new RuntimeBinderException("Debe crear una nueva tripulación");
            }
            if (t.cedula == null)
            {
                throw new RuntimeBinderException("Cédula requerido");
            }
            if (t.nombre == null)
            {
                throw new RuntimeBinderException("Nombre requerido");
            }
            if (t.fechaNacimiento == null)
            {
                throw new RuntimeBinderException("Fehca de nacimiento requerida");
            }
            if (t.aerolinea == null)
            {
                throw new RuntimeBinderException("Aerolinea requerida");
            }
            if (t.rol == null)
            {
                throw new RuntimeBinderException("Rol requerido");
            }
            if (t.estado == null)
            {
                throw new RuntimeBinderException("Estado requerido");
            }
            if (t.id <= 0)
            {
                return new TripulacionDAO().insertar(t);
            }
            else
            {
                //return new ColaboradorDAO().editar(t);
                return false;
            }
        }
        public List<Tripulacion> cargar()
        {
            return new TripulacionDAO().seleccionar();
        }

        public bool cambiarEstado(int id)
        {
            return new TripulacionDAO().CambiarEstado(id);
        }

        public List<Tripulacion> cargarPilotos(int id)
        {
            return new TripulacionDAO().seleccionarPiloto(id);
        }

        public List<Tripulacion> cargarServicio(int id)
        {
            return new TripulacionDAO().seleccionarServicio(id);
        }

        public List<Tripulacion> cargarTripulacionPorAerolinea(int id)
        {
            return new TripulacionDAO().cargarTripulacionPorAerolinea(id);
        }
    }
}
