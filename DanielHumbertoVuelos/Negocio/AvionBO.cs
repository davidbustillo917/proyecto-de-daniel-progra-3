﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DanielHumbertoVuelos.Datos;
using DanielHumbertoVuelos.Entidades;
using Microsoft.CSharp.RuntimeBinder;

namespace DanielHumbertoVuelos.Negocio
{
    class AvionBO
    {
        public bool registrar(Avion av)
        {
            if (av == null)
            {
                throw new RuntimeBinderException("Debe crear un nuevo avión");
            }
            if (av.modelo== null)
            {
                throw new RuntimeBinderException("Modelo requerido");
            }
            if (av.annoConstruccion == null)
            {
                throw new RuntimeBinderException("Año de construccion requerido");
            }
            if (av.aerolinea== null)
            {
                throw new RuntimeBinderException("Aerolinea requerido");
            }
            if (av.capacidad == 0)
            {
                throw new RuntimeBinderException("Capacidad requerida");
            }
            if (av.estado == null)
            {
                throw new RuntimeBinderException("Estado requerido");
            }
            if (av.id <= 0)
            {
                return new AvionDAO().insertar(av);
            }
            else
            {
                //return new ColaboradorDAO().editar(cola);
                return false;
            }
        }

        public List<Avion> cargar()
        {
            return new AvionDAO().seleccionar();
        }

        public List<Avion> cargarParaRandom(int id)
        {
            return new AvionDAO().seleccionarParaRandom(id);
        }
        public List<Avion> cargarAvionesPorRanking()
        {
            return new AvionDAO().cargarAvionesPorRanking();
        }

        public List<Avion> cargarPorAerolinea(int id, int idAerolinea)
        {
            return new AvionDAO().seleccionarPorAerolinea(id, idAerolinea);
        }

        public Avion cargarAvionPorAerolinea(int id, int idAerolinea)
        {
            return new AvionDAO().cargarAvionPorAerolinea(id, idAerolinea);
        }

        public bool cambiarEstadoAvion(int id)
        {
            return new AvionDAO().CambiarEstadoAvion(id);
        }
    }
}
